use mysqltutorial;


/* Diese Abfrage könnte auch zwei Datensätze zurückgeben, wenn es zwei mit dem höchsten amount gibt */
SELECT 
    customerNumber, 
    checkNumber, 
    amount,
    paymentDate,
    customerNumber
FROM
    payments
WHERE
    amount = (SELECT MAX(amount) FROM payments);


/* Hier wird noch ein payment mit dem maximalen Betrag erstellt. Dadurch gibt es jetzt 2 maximale */
INSERT INTO
    payments
    (customerNumber, checkNumber, amount, paymentDate)
VALUES
    (141, 'YY890321', 120166.58, '2005-03-18');

/* Selber SELECT wie oben liefert nun zwei Ergebnisse */
SELECT 
    customerNumber, 
    checkNumber, 
    amount,
    paymentDate,
    customerNumber
FROM
    payments
WHERE
    amount = (SELECT MAX(amount) FROM payments);

/* Erstellten Eintrag wieder raus löschen */
DELETE FROM
    payments
WHERE
    checkNumber = 'YY890321';




/* Fehlerhafte Abfrage, weil db nicht weiß, welche checkNumber ausgegeben werden soll */
SELECT checkNumber, MAX(amount) FROM payments;