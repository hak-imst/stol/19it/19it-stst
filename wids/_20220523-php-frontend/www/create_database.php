<?php

$host = "serp-mysql";
$user = "root";
$pass = "123";
$database = "serp";

$running = true;
while($running){
  try {
    $conn = new PDO('mysql:host='.$host.';charset=utf8', $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $running = false;
  }
  catch(PDOException $e) {
    //echo "Connection to $host failed: " . $e->getMessage();
    sleep(1);
  }

}

/**
 * Select Database
 */
try {
    $conn->exec('USE '.$database);
  }
  catch(PDOException $e) {
    // if not possible to select, create Database
      $message[] = 'Database "'.$database.'" does not exist. Creating database ...';
      $sql = file_get_contents('database.sql');
      $qr = $conn->exec($sql);
    try {
      $conn->exec('USE '.$database);
    }
    catch(PDOException $e2) {
      echo "Setting up database failed: " . $e2->getMessage();
    }
  }



?>

